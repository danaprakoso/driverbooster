<?php

include "Util.php";

class User extends CI_Controller {
	
	public function get_garages() {
		$lat = doubleval($this->input->post('lat'));
		$lng = doubleval($this->input->post('lng'));
		$garages = $this->db->query("SELECT *, SQRT(POW(69.1 * (latitude - " . $lat . "), 2) + POW(69.1 * (" . $lng . " - longitude) * COS(latitude / 57.3), 2)) AS distance FROM `garages` ORDER BY distance")->result_array();
		echo json_encode($garages);
	}
	
	public function get_oprekers() {
		$lat = doubleval($this->input->post('lat'));
		$lng = doubleval($this->input->post('lng'));
		$oprekers = $this->db->query("SELECT *, SQRT(POW(69.1 * (latitude - " . $lat . "), 2) + POW(69.1 * (" . $lng . " - longitude) * COS(latitude / 57.3), 2)) AS distance FROM `oprekers` ORDER BY distance")->result_array();
		echo json_encode($oprekers);
	}
	
	public function get_garage_by_id() {
		$id = $this->input->post('id');
		echo json_encode($this->db->query("SELECT * FROM `garages` WHERE `id`=" . $id)->row_array());
	}
	
	public function get_opreker_by_id() {
		$id = $this->input->post('id');
		echo json_encode($this->db->query("SELECT * FROM `oprekers` WHERE `id`=" . $id)->row_array());
	}
	
	public function check_email_phone() {
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$userCount = $this->db->query("SELECT * FROM `users` WHERE `email`='" . $email . "'")->num_rows();
		if ($userCount > 0) {
			echo json_encode(array(
				'response_code' => -1
			));
			return;
		}
		$userCount = $this->db->query("SELECT * FROM `users` WHERE `phone`='" . $phone . "'")->num_rows();
		if ($userCount > 0) {
			echo json_encode(array(
				'response_code' => -2
			));
			return;
		}
		echo json_encode(array(
			'response_code' => 1
		));
	}
	
	public function login() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$users = $this->db->query("SELECT * FROM `users` WHERE `email`='" . $email . "' AND `password`='" . $password . "'")
			->result_array();
		if (sizeof($users) > 0) {
			$user = $users[0];
			$active = intval($user['active']);
			if ($active == 1) {
				$user['response_code'] = 1;
			} else {
				$user['response_code'] = -2;
			}
			echo json_encode($user);
		} else {
			echo json_encode(array(
				'response_code' => -1
			));
		}
	}
	
	public function signup() {
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$name = $this->input->post('name');
		$signupNeedConfirmation = intval($this->db->get('settings')->row_array()['signup_need_confirmation']);
		$this->db->insert('users', array(
			'email' => $email,
			'phone' => $phone,
			'password' => $password,
			'name' => $name,
			'active' => ($signupNeedConfirmation==1)?0:1
		));
		echo json_encode(array(
			'response_code' => 1,
			'signup_need_confirmation' => $signupNeedConfirmation
		));
	}

	public function send_verification_email() {
		$email = $this->input->post('email');
		$code = $this->input->post('code');
		Util::send_email($email, "Kode verifikasi Anda: " . $code, "Masukkan 6-digit kode verifikasi berikut ke kotak yang tersedia: <b>" . $code . "</b>");
	}
	
	public function reset_password() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$this->db->query("UPDATE `users` SET `password`='" . $password . "' WHERE `email`='" . $email . "'");
	}
	
	public function get_nearby_users() {
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		$users = $this->db->query("SELECT *, SQRT(POW(69.1 * (latitude - " . $lat . "), 2) + POW(69.1 * (" . $lng . " - longitude) * COS(latitude / 57.3), 2)) AS distance FROM `users` HAVING distance < 50 ORDER BY distance")->result_array();
		echo json_encode($users);
	}
	
	public function update_location() {
		$id = $this->input->post('id');
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		$this->db->query("UPDATE `users` SET `latitude`=" . $lat . ", `longitude`=" . $lng . " WHERE `id`=" . $id);
		echo "UPDATE `users` SET `latitude`=" . $lat . ", `longitude`=" . $lng . " WHERE `id`=" . $id;
	}
	
	public function add_gofood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$countryCode = $this->input->post('country_code');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0;
			$lng = 0;
			$restaurantID = "";
			if ($countryCode == "ID") {
				$location = $restaurant['content']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_id'];
			} else if ($countryCode == "TH") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			} else if ($countryCode == "VN") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			}
			$this->db->query("DELETE FROM `gofood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('gofood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'country_code' => $countryCode,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_grabfood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = $restaurant['latlng']['latitude'];
			$lng = $restaurant['latlng']['longitude'];
			$restaurantID = $restaurant['id'];
			$this->db->query("DELETE FROM `grabfood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('grabfood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_shopeefood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = doubleval($restaurant['store']['location']['latitude']);
			$lng = doubleval($restaurant['store']['location']['longitude']);
			$restaurantID = $restaurant['store']['id'];
			$this->db->query("DELETE FROM `grabfood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('shopeefood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_traveloka_food_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0; // latitude not available
			$lng = 0; // longitude not available
			$restaurantID = $restaurant['restaurantId'];
			$this->db->query("DELETE FROM `traveloka_food_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('traveloka_food_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_atm() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allAtms = json_decode($this->input->post('atms'), true);
		for ($i=0; $i<sizeof($allAtms); $i++) {
			$atm = $allAtms[$i];
			$lat = $atm['position'][0];
			$lng = $atm['position'][1];
			$atmID = $atm['id'];
			$this->db->query("DELETE FROM `atms` WHERE `atm_id`='".$atmID."'");
			$this->db->insert('atms', array(
				'atm_id' => $atmID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($atm),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = $restaurant['position'][0];
			$lng = $restaurant['position'][1];
			$restaurantID = $restaurant['id'];
			$this->db->query("DELETE FROM `restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function is_gofood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_grabfood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `grabfood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_shopeefood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `shopeefood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_traveloka_food_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_atm_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `atms` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_restaurant_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function get_gofood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_grabfood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `grabfood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_shopeefood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `shopeefood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_traveloka_food_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_atms() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `atms` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function is_new_user_need_confirmation() {
		echo $this->db->get('settings')->row_array()['signup_need_confirmation'];
	}
	
	public function set_new_user_need_confirmation() {
		$newUserNeedConfirmation = intval($this->input->post('new_user_need_confirmation'));
		$this->db->update('settings', array(
			'signup_need_confirmation' => $newUserNeedConfirmation
		));
	}
}
