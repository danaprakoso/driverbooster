<?php

class FCM {

	public static function send_notification_to_topic_all_types($title, $body, $link, $imgURL, $topic, $data) {
		$url = 'https://fcm.googleapis.com/fcm/send';
	    $fields = array(
            'to' => '/topics/' . $topic,
            'notification' => array(
            	'title' => $title,
            	'body' => $body,
            	'click_action' => 'openlink',
            	'image' => $imgURL
            )
    	);
    	$data['link'] = $link;
    	if (sizeof($data) > 0) {
    		$fields['data'] = $data;
    	}
    	$fields = json_encode($fields);
    	$headers = array (
            'Authorization: key=' . "AAAAQV_ZBDs:APA91bFNV1Ic8xMAEzsyLDvD-KB3dhGe2SKSPYGykTGP3y2uoWPpaW4MplTJ85s4LH0xsVfmWGbSa9puSCkloAVHxnDae9-Yz8J53SQA7qnJFyYddLdjKbpQul_31z1cZAxJkLhd9cGI",
            'Content-Type: application/json'
    	);
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	    $result = curl_exec ( $ch );
	    curl_close ( $ch );
	}

	public static function send_notification_to_topic($title, $body, $topic, $data) {
		$url = 'https://fcm.googleapis.com/fcm/send';
	    $fields = array(
            'to' => '/topics/' . $topic,
            'notification' => array(
            	'title' => $title,
            	'body' => $body
            )
    	);
    	if (sizeof($data) > 0) {
    		$fields['data'] = $data;
    	}
    	$fields = json_encode($fields);
    	$headers = array (
            'Authorization: key=' . "AAAAQV_ZBDs:APA91bFNV1Ic8xMAEzsyLDvD-KB3dhGe2SKSPYGykTGP3y2uoWPpaW4MplTJ85s4LH0xsVfmWGbSa9puSCkloAVHxnDae9-Yz8J53SQA7qnJFyYddLdjKbpQul_31z1cZAxJkLhd9cGI",
            'Content-Type: application/json'
    	);
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	    $result = curl_exec ( $ch );
	    curl_close ( $ch );
	}

	public static function send_link_notification_to_topic($title, $body, $link, $topic, $data) {
		$url = 'https://fcm.googleapis.com/fcm/send';
	    $fields = array(
            'to' => '/topics/' . $topic,
            'notification' => array(
            	'title' => $title,
            	'body' => $body,
            	'click_action' => 'openlink'
            )
    	);
    	$data['link'] = $link;
    	if (sizeof($data) > 0) {
    		$fields['data'] = $data;
    	}
    	$fields = json_encode($fields);
    	$headers = array (
            'Authorization: key=' . "AAAAQV_ZBDs:APA91bFNV1Ic8xMAEzsyLDvD-KB3dhGe2SKSPYGykTGP3y2uoWPpaW4MplTJ85s4LH0xsVfmWGbSa9puSCkloAVHxnDae9-Yz8J53SQA7qnJFyYddLdjKbpQul_31z1cZAxJkLhd9cGI",
            'Content-Type: application/json'
    	);
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	    $result = curl_exec ( $ch );
	    curl_close ( $ch );
	}

	public static function send_image_notification_to_topic($title, $body, $image, $topic, $data) {
		$url = 'https://fcm.googleapis.com/fcm/send';
	    $fields = array(
            'to' => '/topics/' . $topic,
            'notification' => array(
            	'title' => $title,
            	'body' => $body,
            	'image' => $image
            )
    	);
    	if (sizeof($data) > 0) {
    		$fields['data'] = $data;
    	}
    	$fields = json_encode($fields);
    	$headers = array (
            'Authorization: key=' . "AAAAQV_ZBDs:APA91bFNV1Ic8xMAEzsyLDvD-KB3dhGe2SKSPYGykTGP3y2uoWPpaW4MplTJ85s4LH0xsVfmWGbSa9puSCkloAVHxnDae9-Yz8J53SQA7qnJFyYddLdjKbpQul_31z1cZAxJkLhd9cGI",
            'Content-Type: application/json'
    	);
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	    $result = curl_exec ( $ch );
	    curl_close ( $ch );
	}
}
